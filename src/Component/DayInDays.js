import React from 'react';
// import '../css/weatherDays/days.css';


class DayInDays extends React.Component {

    render() {
        const {day, icon, max_temp, min_temp, date} = this.props


        return (
            <>

            <div className="days-day" >
                <div className="day" >{day}</div>
                <img className="img-day" src={icon} />
                <div className="temp" >
                    <div>{max_temp} c</div>
                    <div>{min_temp} c</div>
                    <div className="date" >{date}</div>
                </div>
            </div>

            </>
        );
    }
}

export default DayInDays