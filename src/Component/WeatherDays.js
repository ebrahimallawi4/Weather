import React from 'react';
import DayInDays from './DayInDays'

import '../css/weatherDays/days.css';


class WeatherDays extends React.Component {

    getDay(_date) {
        switch (_date.getDay()) {
            case 0:
                return "Sunday";
                break;
            case 1:
                return "Monday";
                break;
            case 2:
                return "Tuesday";
                break;
            case 3:
                return "Wednesday";
                break;
            case 4:
                return "Thursday";
                break;
            case 5:
                return "Friday";
                break;
            case 6:
                return "Saturday";
        }
    }
    render() {
        
        const {weather} = this.props
        // console.log(weather['forecast'])
        const threeDays = weather['forecast']['forecastday'].map(forecast => {
            var d = forecast['date']
            var date  = new Date(d)
            return(
                <>
                <DayInDays 
                day={this.getDay(date)}
                icon={weather['current']['condition']['icon']}
                max_temp={forecast['day']['maxtemp_c']}
                min_temp={forecast['day']['mintemp_c']}
                date={forecast['date']}
                />

                </>
            );
        })

        return (
            <>
                <div className="days" >
                    {threeDays}
                </div>
            </>
        );
    }
}

export default WeatherDays