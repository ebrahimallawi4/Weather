import React from 'react'

import '../css/weatherOneDay/weatherOneDay.css';

class WeatherOneDay extends React.Component {

    render() {

        const {weather} = this.props
        


        return (
            <>
                <div className="name-city" >{weather['location']['name']}</div>
                <div className="name-city" >Today</div>
                <div className="weather-day" >
                    <img className="image-day" src={weather['current']['condition']['icon']} />
                    <div className="temp-day" >Now {weather['current']['temp_c']} C</div>
                    <div className="forecast-day" >
                        <div>{weather['forecast']['forecastday'][0]['day']['maxtemp_c']} c</div>
                        <div>{weather['forecast']['forecastday'][0]['day']['mintemp_c']} c</div>
                        <div className="date" >{weather['location']['localtime']}</div>
                    </div>
                </div>
                <hr className="hr" />
            </>
        );
    }
}

export default WeatherOneDay