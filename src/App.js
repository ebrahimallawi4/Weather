import React from 'react';
// import logo from './logo.svg';
import Controller from './Component/Controller'
import WeatherOneDay from './Component/WeatherOneDay'
import WeatherDays from './Component/WeatherDays'
import $ from 'jquery'

import './css/app/app.css'




class App extends React.Component {
  constructor() {
    super()
    // this.getWeather = this.getWeather.bind(this)
    this.ref = React.createRef();
  }
  state = {
    city: 'Saydnaya',
    dt: Date.now(),
    allData: [],
    threeDays: [],
    error: []
  }
  
  async getWeather() {
    
    var response = await fetch( `https://weatherapi-com.p.rapidapi.com/forecast.json?q=${this.state.city}&dt=${this.state.dt}`, {
      "method": "GET",
      "headers": {
        "x-rapidapi-key": "ab223eaa3fmshfe0f8f54b620d2ep16628djsn07279fee6913",
        "x-rapidapi-host": "weatherapi-com.p.rapidapi.com"
      }
    })

    var data = await response.json()
    console.log(data['error'])
    if(data['error']) {
      this.setState({error: data['error']})
    }
    this.setState({allData: data})

  }

  async getThreeDays() {
    var response = await fetch( `https://weatherapi-com.p.rapidapi.com/forecast.json?q=${this.state.city}&days=3&dt=${this.state.dt}`, {
      "method": "GET",
      "headers": {
        "x-rapidapi-key": "ab223eaa3fmshfe0f8f54b620d2ep16628djsn07279fee6913",
        "x-rapidapi-host": "weatherapi-com.p.rapidapi.com"
      }
    })

    var data = await response.json()
    // console.log(data)
    this.setState({threeDays: data})


  }

  async changeState() {
    var _city = $('#city').val()
    await this.setState({city: _city})
    {await this.getWeather()}
    {await this.getThreeDays()}
    // console.log($('#date').val())
    console.log(this.state.allData)
  }
  

  

  async componentWillMount() {
    {await this.getWeather()}
    {await this.getThreeDays()}
  }
  
  render() {
      if(this.state.allData == '' || this.state.threeDays == '') {
        return (
          <h1>loading</h1>
        )
      }
      else if(this.state.error['code'] == 1006) {
        console.log(this.state.error)
        return(
          <>
          <h1>{this.state.error['message']}</h1>
          </>
        )
      }
      else {
        return (
          <div className="contain-weather">
            <div className="NameApp" >Weather</div>
            <Controller change={() => this.changeState()} />
            <WeatherOneDay weather={this.state.allData} />
            <WeatherDays weather={this.state.threeDays} />
          </div>
        );
        
      }



  }
}

export default App;
